"""
This file is loaded automatically by micropython after bootup.
Set the DEVICE_IS_MASTER to True if this is a master node device,
otherwise set it to False.
"""

DEVICE_IS_MASTER = True

if DEVICE_IS_MASTER:
    import master
else:
    import slave
