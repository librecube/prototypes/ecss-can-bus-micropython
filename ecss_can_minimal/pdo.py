from .message import Message
from .constants import STATE_CODE_OPERATIONAL


class Pdo:
    def __init__(self, node,
                 tpdo_index, tpdo_map_index, rpdo_index, rpdo_map_index):
        self.node = node
        self.tpdo_index = tpdo_index
        self.tpdo_map_index = tpdo_map_index
        self.rpdo_index = rpdo_index
        self.rpdo_map_index = rpdo_map_index

    def transmit(self, node_id):
        """Send a TPDO."""
        if self.node.state != STATE_CODE_OPERATIONAL:
            return False

        cob_id = self.node.od[self.tpdo_index][1].value + node_id
        map_record = self.node.od[self.tpdo_map_index]
        no_of_mappings = map_record[0].value

        data = []
        # go through all mapping entries
        for i in range(1, no_of_mappings + 1):
            s = map_record[i].value  # mapping is recorded as string
            od_index = int(s[0:4], 16)  # extract od index from hex string
            od_subindex = int(s[4:6], 16)
            length = int(s[6:8], 16)
            octets = int(length / 8)
            # read the value as stored in the od at given location
            try:
                val = self.node.od[od_index][od_subindex].value
            except:
                # if the index is not defined, return without sending
                print("error: od entry at [0x{:04x}][0x{:02x}] is not defined".format(
                    od_index, od_subindex))
                return False
            # append the value bytewise to data list
            for octet in range(octets):
                # append data with LSB first
                data.append((val >> octet * 8) & 0xff)

        self.message = Message(can_id=cob_id, data=data)
        self.node.network.send_message(self.message)
        return True
