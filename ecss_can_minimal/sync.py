import pyb

from .message import Message
from .constants import COB_ID_SYNC, TIMER_SYNC, LED_SYNC
from .constants import OD_INDEX_COMMUNICATION_CYCLE_PERIOD


class SyncProducer:
    """Transmit a SYNC message periodically."""
    def __init__(self, node):
        self.node = node
        self.message = Message(can_id=COB_ID_SYNC, data=None)
        self.timer = pyb.Timer(TIMER_SYNC)
        self.period = None
        self.running = False

    def send(self):
        """Send out a frame once."""
        self.node.network.send_message(self.message)

    def callback(self, timer):
        """The timer triggers this function to send the SYNC message."""
        if LED_SYNC:
            pyb.LED(LED_SYNC).toggle()
        self.send()

    def start(self):
        """Start periodic transmission."""
        tmp = self.node.od[OD_INDEX_COMMUNICATION_CYCLE_PERIOD]
        if tmp is not None:
            self.period = tmp.value / 1000000

        if not self.period:
            raise ValueError("No valid transmission period.")

        if self.running:
            self.stop()

        self.timer.init(freq=1/self.period)
        self.timer.callback(self.callback)
        self.running = True

    def stop(self):
        """Stop periodic transmission."""
        if self.running:
            self.timer.deinit()
            self.timer.callback(None)
            self.running = False


class SyncConsumer:
    """Listen for SYNC message."""
    def __init__(self, node):
        self.node = node
        self.running = False

    def start(self):
        """Start listening for SYNC transmission."""
        self.running = True

    def stop(self):
        """Stop periodic transmission."""
        if self.running:
            self.running = False

    def callback(self):
        """This callback gets called upon SYNC reception. Overwrite it with
        a function to be executed when receiving a SYNC signal."""
        pass

    def received(self):
        """Gets executed with each SYNC reception."""
        if self.running:
            if LED_SYNC:
                pyb.LED(LED_SYNC).toggle()
            if callable(self.callback):
                self.callback()
