from ecss_can_minimal.objectdictionary import ObjectDictionary
from ecss_can_minimal.objectdictionary import Variable, Record
from ecss_can_minimal.constants import *


MASTER_NODE_ID = 1
SLAVE_NODE_ID = 2


class MasterNodeObjectDictionary(ObjectDictionary):
    def __init__(self):
        super().__init__(MASTER_NODE_ID)

        # define SYNC object
        super().add_object(OD_INDEX_COMMUNICATION_CYCLE_PERIOD,
                           Variable(1000000, "Communic. cycle period (usec)"))

        # define producer and consumer heartbeat configuration
        super().add_object(OD_INDEX_PRODUCER_HEARTBEAT_TIME,
                           Variable(250, "Producer heartbeat time (msec)"))
        super().add_object(OD_INDEX_CONSUMER_HEARTBEAT_TIME,
                           Variable(500, "Consumer heartbeat time (msec)"))

        # redundancy management
        self.bdefault = Variable(1, "Bdefault")
        rec = Record("Redundancy management")
        rec.add_member(0, Variable(1))  # 1 subindex
        rec.add_member(1, Variable(self.bdefault, "Bdefault"))
        super().add_object(OD_INDEX_REDUNDANCY_MANAGEMENT, rec)

        # TPDO1 for SCET
        rec = Record("TPDO1 parameter")
        rec.add_member(0, Variable(2))
        rec.add_member(1, Variable(COB_ID_TPDO_1))
        rec.add_member(2, Variable(254, "transmission type"))
        super().add_object(OD_INDEX_TPDO1, rec)

        rec = Record("TPDO1 mapping")
        rec.add_member(0, Variable(3))
        rec.add_member(1, Variable("{:04x}".format(OD_INDEX_SCET) + "01" + "10",
                                   "fill zeros"))
        rec.add_member(2, Variable("{:04x}".format(OD_INDEX_SCET) + "02" + "08",
                                   "SCET fine time mapping"))
        rec.add_member(3, Variable("{:04x}".format(OD_INDEX_SCET) + "03" + "20",
                                   "SCET coarse time mapping"))
        super().add_object(OD_INDEX_TPDO1_MAPPING, rec)

        self.scet_fine = Variable(0x00)  # 1 byte
        self.scet_coarse = Variable(0x00000000)  # 4 bytes
        rec = Record("Local SCET Get object")
        rec.add_member(0, Variable(3))  # 3 subindeces
        rec.add_member(1, Variable(0x0000))
        rec.add_member(2, self.scet_fine)
        rec.add_member(3, self.scet_coarse)
        super().add_object(OD_INDEX_SCET, rec)

        # TPDO2 for TC
        rec = Record("TPDO2 parameter")
        rec.add_member(0, Variable(2))
        rec.add_member(1, Variable(COB_ID_RPDO_2))
        rec.add_member(2, Variable(254, "transmission type"))
        super().add_object(OD_INDEX_TPDO2, rec)

        rec = Record("TPDO2 mapping")
        rec.add_member(0, Variable(2))
        rec.add_member(1, Variable("{:04x}".format(OD_INDEX_TC) + "01" + "10",
                                   "Command code mapping"))
        rec.add_member(2, Variable("{:04x}".format(OD_INDEX_TC) + "02" + "30",
                                   "Command parameters mapping"))
        super().add_object(OD_INDEX_TPDO2_MAPPING, rec)

        self.tc_code = Variable(0x0000)  # 2 bytes
        self.tc_param = Variable(0x000000000000)  # max 6 bytes
        rec = Record("Send TC")
        rec.add_member(0, Variable(2))  # 2 subindeces
        rec.add_member(1, self.tc_code)
        rec.add_member(2, self.tc_param)
        super().add_object(OD_INDEX_TC, rec)

        # TPDO3 for TM_REQUEST
        rec = Record("TPDO3 parameter")
        rec.add_member(0, Variable(2))
        rec.add_member(1, Variable(COB_ID_RPDO_3))
        rec.add_member(2, Variable(254, "transmission type"))
        super().add_object(OD_INDEX_TPDO3, rec)

        rec = Record("TPDO3 mapping")
        rec.add_member(0, Variable(1))
        rec.add_member(1, Variable("{:04x}".format(OD_INDEX_TM) + "01" + "10",
                                   "TM request mapping"))
        super().add_object(OD_INDEX_TPDO3_MAPPING, rec)

        self.tm_request = Variable(0x0000)  # 2 bytes
        rec = Record("Send TM_REQUEST")
        rec.add_member(0, Variable(1))  # 1 subindeces
        rec.add_member(1, self.tm_request)
        super().add_object(OD_INDEX_TM, rec)

        # TPDO4 not used
        rec = Record("TPDO4 parameter")
        rec.add_member(0, Variable(2))
        rec.add_member(1, Variable(None))
        rec.add_member(2, Variable(254, "transmission type"))
        super().add_object(OD_INDEX_TPDO4, rec)

        rec = Record("TPDO4 mapping")
        rec.add_member(0, Variable(None))
        super().add_object(OD_INDEX_TPDO4_MAPPING, rec)
